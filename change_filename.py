import os
import sys

count = int(sys.argv[1])

old_img_path = sys.argv[2]
new_img_path = sys.argv[3]
old_note_path = sys.argv[4]
new_note_path = sys.argv[5]

for filename in [x for x in os.listdir(old_img_path) if x != 'info']:
    extension = "." + filename.split(".")[1]
    new_name = str(count) + extension
    note_name = str(filename.split(".")[0]) + ".txt"
    new_note = str(count) + ".txt"
    os.rename(old_img_path+filename, new_img_path+new_name)
    os.rename(old_note_path+note_name, new_note_path+new_note)
    count += 1
    